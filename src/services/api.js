import {ax} from '@/config/axios'
import config from '@/config/'

export const login = params => ax.post('/employee/login', params) // 用户登录
export const getParams = params => ax.get('', {params: params}) // 模板
export const postParams = params => ax.post('', params)
export const postParamsJson = params => ax.post('', params, {headers: {'Content-type': 'application/json;utf-8'}}) // 订单 对账