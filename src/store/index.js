import Vue from 'vue'
import Vuex from 'vuex'

// Vuex 模块引入
import app from '@/store/modules/app'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app
  }
})
