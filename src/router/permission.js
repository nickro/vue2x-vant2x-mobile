import router from "@/router";

router.beforeEach(
  async (
    to,
    from,
    next
  ) => {
    document.title = to.meta.title;
    // 获取token
    const token = localStorage.getItem('token')
    if (token) {
      if (to.path === "/login") {
        next({ path: "/" });
      } else {
        next();
      }
    } else {
      if (to.meta.isOpen) {
        // 开放页面，无需验证
        next();
      } else {
        next(`/login?redirect=${to.path}`);
      }
    }
  }
);
